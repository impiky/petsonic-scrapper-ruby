require 'rubygems'
require 'curb'
require 'nokogiri'
require 'csv'


puts("Provide link to parse")
url_to_parse =gets.chomp
puts("Provide csv file name")
filename = gets.chomp


def parse_links(link)
    puts("Parsing category links")
    products_category = Array.new
    page_number = 1
    while true
        if page_number == 1
            url = Curl.get(link) do |url| # getting first page
                url.headers['User-Agent'] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36"
            end
        else 
            url = Curl.get(link + "?p=" + page_number.to_s) do |url| # getting rest pages with pagination
                url.headers['User-Agent'] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36"
            end
        end
        html = Nokogiri::HTML(url.body_str)
        last_page = html.search("//*[@id=\"pagination_next_bottom\"]")
        html.search("//*[@id=\"product_list\"]/li/div/div/div[1]/a/@href").each do |node| # getting product links
            node.map {|attr| attr.value} # getting href value
            products_category <<  node.text 
        end 
        if last_page.to_s.include? "disabled" #checking if reached last page
            break
        end
        page_number += 1
    end
return products_category
end

def info_parsing_and_csv_writing(product_links, csv_filename)
puts("parsing title,image,price,weight and writing into csv file(it takes some time)")
image = nil
weight = nil
price = nil
title_and_weight = nil
i = 0
CSV.open(csv_filename + ".csv", "w", :write_headers=> true,:headers => true) do |csv|
    until product_links.length == product_links[i] do
        if Curl.get(product_links[i]).header_str.include? "302" and product_links[i += 1] != nil # checking if redirected to not product page
            next
        elsif product_links[i] == nil
            break
        else
            product_url = Curl.get(product_links[i]) do |url|
                url.headers['User-Agent'] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36"
            end
            html = Nokogiri::HTML(product_url.body_str)
            html.search("//*[@id=\"bigpic\"]/@src").each do |node| # getting product picture
            node.map {|attr| attr.value}
            image =  node.text
            end
            html.search("//*[@id=\"center_column\"]/div/div/div[2]/div[2]/h1").each do |node| # getting product title
                title = node.text
                html.search("//*[@id=\"attributes\"]/fieldset/div/ul/li").each do |node| # gettiong product price and weight
                    weight, price = node.text.strip.split(/(\s\d.+)/) # getting weight and price separately
                    title_and_weight = title + " - " + weight
                    csv << [title_and_weight, price, image] # pushing all data into csv file
                    end   
                end        
            end
        i += 1  
        end
    end
puts("Done! File " + csv_filename + ".csv" + " saved in program directory")
end


product_links = parse_links(url_to_parse)
info_parsing_and_csv_writing(product_links, filename)


# https://www.petsonic.com/snacks-huesos-para-perros/
